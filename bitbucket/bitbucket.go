// Package bitbucket defines some types that are used in the payload sent by
// BitBucket's POST hook.
package bitbucket

type Link struct {
	Self struct {
		Href string `json:"href"`
	} `json:"self"`
	Html struct {
		Href string `json:"href"`
	} `json:"html"`
	Commits struct {
		Href string `json:"href"`
	} `json:"commits"`
	Avatar struct {
		Href string `json:"href"`
	} `json:"avatar"`
}

type User struct {
	Username    string `json:"username"`
	DisplayName string `json:"display_name"`
	Uuid        string `json:"uuid"`
	Links       Link   `json:"links"`
}

type Repository struct {
	Name     string `json:"name"`
	FullName string `json:"full_name"`
	Uuid     string `json:"uuid"`
	Links    Link   `json:"links"`
}

type RefState struct {
	Type   string `json:"type"`
	Name   string `json:"name"`
	Target struct {
		Type    string `json:"type"`
		Hash    string `json:"hash"`
		Author  User   `json:"author"`
		Message string `json:"message"`
		Date    string `json:"date"`
		Parents []struct {
			Type  string `json:"type"`
			Hash  string `json:"hash"`
			Links Link   `json:"links"`
		}
		Links Link `json:"links"`
	}
}

type Commit struct {
	New     RefState `json:"new"`
	Old     RefState `json:"old"`
	Links   Link     `json:"links"`
	Created bool     `json:"created"`
	Closed  bool     `json:"closed"`
	Forced  bool     `json:"forced"`
}

type PushPayload struct {
	User       User       `json:"actor"`
	Repository Repository `json:"repository"`
	Push       struct {
		Changes []Commit `json:"changes"`
	} `json:"push"`
}
