// Package gitlab defines some types that are used in the payload sent by
// GitLab's POST hook.
package gitlab

type User struct {
	Name  string `json:"name"`
	Email string `json:"email"`
}

type Repo struct {
	Name        string `json:"name"`
	Url         string `json:"url"`
	Description string `json:"description"`
	Homepage    string `json:"homepage"`
}

type Commit struct {
	Id        string `json:"id"`
	Message   string `json:"message"`
	Timestamp string `json:"timestamp"`
	Url       string `json:"url"`
	Author    User   `json:"author"`
}

type PushData struct {
	Before            string   `json:"before"`
	After             string   `json:"after"`
	Ref               string   `json:"ref"`
	UserId            int      `json:"user_id"`
	Username          string   `json:"user_name"`
	ProjectId         int      `json:"project_id"`
	Repository        Repo     `json:"repository"`
	Commits           []Commit `json:"commit"`
	TotalCommitsCount int      `json:"total_commits_count"`
	CIYamlFile        string   `json:"ci_yaml_file"`
}

type PushPayload struct {
	BuildId         int      `json:"build_id"`
	BuildName       string   `json:"build_name"`
	BuildStatus     string   `json:"build_status"`
	BuildStartedAt  string   `json:"build_started_at"`
	BuildFinishedAt string   `json:"build_finished_at"`
	ProjectId       int      `json:"project_id"`
	ProjectName     string   `json:"project_name"`
	GitlabUrl       string   `json:"gitlab_url"`
	Ref             string   `json:"ref"`
	Sha             string   `json:"sha"`
	BeforeSha       string   `json:"before_sha"`
	PushData        PushData `json:"push_data"`
}
