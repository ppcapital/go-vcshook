package github

import (
	"bytes"
	"encoding/json"
	"net/http"
)

// ParsePushPayload will parse the request sent by GitHub and return the
// POST'ed payload if possible.
func ParsePushPayload(r *http.Request) (PushPayload, error) {
	var payload PushPayload

	// parse the form first and foremost
	err := r.ParseForm()
	if err != nil {
		return payload, err
	}

	decoder := json.NewDecoder(r.Body)
	defer r.Body.Close()

	err = decoder.Decode(&payload)
	if err != nil {
		// if we fail to parse the payload directly, try extracting it from the
		// request form (for older repos)
		body := bytes.NewBufferString(r.PostFormValue("payload"))

		err = json.Unmarshal(body.Bytes(), &payload)
		if err != nil {
			return payload, err
		}
	}

	return payload, nil
}
