package main

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"bitbucket.org/ppcapital/go-vcshook/bitbucket"
	"bitbucket.org/ppcapital/go-vcshook/github"
	"bitbucket.org/ppcapital/go-vcshook/gitlab"
)

var spec []syncSpec

type syncSpec struct {
	BitBucketRepo string `json:"bitbucket"`
	GitHubRepo    string `json:"github"`
	GitLabRepo    string `json:"gitlab"`
}

func main() {
	if len(os.Args) != 2 {
		log.Fatal("Sync spec missing!")
	}

	specData, err := ioutil.ReadFile(os.Args[1])
	if err != nil {
		log.Fatal("Failed to read sync spec!")
	}
	err = json.Unmarshal(specData, &spec)
	if err != nil {
		log.Fatal("Failed to parse sync spec!")
	}
	log.Printf("WAT: %v", spec)

	http.HandleFunc("/bitbucket/push", handleBitBucketPush)
	http.HandleFunc("/github/push", handleGitHubPush)
	http.HandleFunc("/gitlab/push", handleGitLabPush)

	log.Fatal(http.ListenAndServe(":6543", nil))
}

func guard(r *http.Request) bool {
	if r.Method != "POST" {
		return false
	}

	return true
}

func handleBitBucketPush(w http.ResponseWriter, r *http.Request) {
	if !guard(r) {
		http.Error(w, "Invalid request method", 405)
		return
	}

	payload, err := bitbucket.ParsePushPayload(r)
	if err != nil {
		log.Println("Failed to parse BitBucket payload:", err.Error())
	}

	gh_repo, err := findMatchingRepo("bitbucket", payload.Repository.FullName)
	if err != nil {
		log.Println("ERROR:", err.Error())
		return
	}

	log.Println("SYNCING WITH github.com", gh_repo)
}

func handleGitHubPush(w http.ResponseWriter, r *http.Request) {
	if !guard(r) {
		http.Error(w, "Invalid request method", 405)
		return
	}

	payload, err := github.ParsePushPayload(r)
	if err != nil {
		log.Println("Failed to parse GitHub payload:", err.Error())
		return
	}

	bb_repo, err := findMatchingRepo("github", payload.Repository.FullName)
	if err != nil {
		log.Println("ERROR:", err.Error())
		return
	}

	log.Println("SYNCING WITH bitbucket.org:", bb_repo)
}

func handleGitLabPush(w http.ResponseWriter, r *http.Request) {
	if !guard(r) {
		http.Error(w, "Invalid request method", 405)
		return
	}

	payload, err := gitlab.ParsePushPayload(r)
	if err != nil {
		log.Println("Failed to parse GitLab payload:", err.Error())
		return
	}

	bb_repo, err := findMatchingRepo("gitlab", payload.PushData.Repository.Name)
	if err != nil {
		log.Println("ERROR:", err.Error())
		return
	}

	log.Println("SYNCING WITH bitbucket.org:", bb_repo)
}

func findMatchingRepo(src, repo string) (string, error) {
	log.Println("Finding match for", src, "repo", repo)
	for _, pair := range spec {
		if src == "bitbucket" && pair.BitBucketRepo == repo {
			return pair.GitHubRepo, nil
		} else if src == "github" && pair.GitHubRepo == repo {
			return pair.BitBucketRepo, nil
		} else if src == "gitlab" && pair.GitLabRepo == repo {
			return pair.BitBucketRepo, nil
		}
	}

	return "", errors.New("No matching repo found")
}
