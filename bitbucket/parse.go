package bitbucket

import (
	"encoding/json"
	"net/http"
)

// ParsePushPayload will parse the request sent by BitBucket and return the
// POST'ed payload if possible.
func ParsePushPayload(r *http.Request) (PushPayload, error) {
	var payload PushPayload

	// decode the JSON
	decoder := json.NewDecoder(r.Body)
	defer r.Body.Close()

	err := decoder.Decode(&payload)
	if err != nil {
		return payload, err
	}

	return payload, nil
}
