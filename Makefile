build:
	GOARCH=386 go build -ldflags "-s"
	upx go-vcshook

deps:
	go get -u bitbucket.org/ppcapital/go-vcshook/bitbucket
	go get -u bitbucket.org/ppcapital/go-vcshook/github
	go get -u bitbucket.org/ppcapital/go-vcshook/gitlab

clean:
	rm -f go-vcshook
